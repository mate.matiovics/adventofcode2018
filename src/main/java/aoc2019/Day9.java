package aoc2019;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;

import static java.lang.Integer.parseInt;
import static java.nio.charset.StandardCharsets.UTF_8;

//TODO: refactor
public class Day9 {

    private static final Map<Long, BiFunction<Long, Long, Long>> FUNCTIONS = Map.of(1L, Long::sum, 2L, (a, b) -> a * b);
    private static final Map<Long, BiPredicate<Long, Long>> PREDICATES = Map.of(
            5L, (a, b) -> a != 0, 6L, (a, b) -> a == 0,
            7L, (a, b) -> a < b, 8L, Long::equals
    );

    public static void main(String[] args) throws URISyntaxException, IOException {
        String input = Files.readString(Path.of(Day9.class.getResource("/2019/advent9input.txt").toURI()), UTF_8).trim();
//        String input = "1102,34915192,34915192,7,4,7,99,0";
        long[] opcodes = Arrays.stream(input.split(","))
                .mapToLong(Long::parseLong)
                .toArray();

        test(opcodes);
    }

    private static void test(long[] originalOc) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        long[] oc = Arrays.copyOf(originalOc, 1000000);
        long i = 0;
        int relBase = 0;
        long current = g(oc, i);
        while (current != 99) {
            long param3Type = current / 10000;
            current = current % 10000;
            long param2Type = current / 1000;
            current = current % 1000;
            long param1Type = current / 100;
            current = current % 100;
            long firstParam;
            if (param1Type == 0)
                firstParam = g(oc, g(oc, i + 1));
            else if (param1Type == 1)
                firstParam = g(oc, i + 1);
            else
                firstParam = g(oc, g(oc, i + 1) + relBase);
            long secondParam;
            if (param2Type == 0)
                secondParam = g(oc, g(oc, i + 2));
            else if (param2Type == 1)
                secondParam = g(oc, i + 2);
            else
                secondParam = g(oc, g(oc, i + 2) + relBase);
            int thirdParam;
            if (param3Type == 2) {
                thirdParam = (int) g(oc, i + 3) + relBase;
            } else {
                thirdParam = (int) g(oc, i + 3);
            }
            if (current == 1 || current == 2) {
                oc[thirdParam] = FUNCTIONS.get(current).apply(firstParam, secondParam);
                i += 4;
            } else if (current == 3) {
                System.out.println("input:");
                if (param1Type == 2) {
                    oc[(int) g(oc, i + 1) + relBase] = parseInt(reader.readLine());
                } else {
                    oc[(int) g(oc, i + 1)] = parseInt(reader.readLine());
                }
                i += 2;
            } else if (current == 4) {
                System.out.println(firstParam);
                i += 2;
            } else if (current == 5 || current == 6) {
                i = PREDICATES.get(current).test(firstParam, -1L) ? secondParam : i + 3;
            } else if (current == 7 || current == 8) {
                oc[thirdParam] = PREDICATES.get(current).test(firstParam, secondParam) ? 1 : 0;
                i += 4;
            } else {
                relBase += firstParam;
                i += 2;
            }
            current = g(oc, i);
        }
    }

    private static long g(long[] array, long index) {
        if (index > Integer.MAX_VALUE) {
            throw new IllegalStateException();
        }
        return array[(int) index];
    }
}
