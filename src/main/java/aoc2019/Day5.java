package aoc2019;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;

import static java.lang.Integer.parseInt;
import static java.nio.charset.StandardCharsets.UTF_8;

public class Day5 {

    private static final Map<Integer, BiFunction<Integer, Integer, Integer>> FUNCTIONS = Map.of(1, Integer::sum, 2, (a, b) -> a * b);
    private static final Map<Integer, BiPredicate<Integer, Integer>> PREDICATES = Map.of(
            5, (a, b) -> a != 0, 6, (a, b) -> a == 0,
            7, (a, b) -> a < b, 8, Integer::equals
    );

    public static void main(String[] args) throws URISyntaxException, IOException {
        String input = Files.readString(Path.of(Day5.class.getResource("/2019/advent5input.txt").toURI()), UTF_8).trim();
        int[] opcodes = Arrays.stream(input.split(","))
                .mapToInt(Integer::parseInt)
                .toArray();
        test(opcodes);
    }

    private static void test(int[] originalOc) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int[] oc = Arrays.copyOf(originalOc, originalOc.length);
        int i = 0;
        int current = oc[i];
        while (current != 99) {
            int param2Type = current / 1000;
            current = current % 1000;
            int param1Type = current / 100;
            current = current % 100;
            int firstParam = param1Type == 0 ? oc[oc[i + 1]] : oc[i + 1];
            int secondParam = param2Type == 0 ? oc[oc[i + 2]] : oc[i + 2];
            if (current == 1 || current == 2) {
                oc[oc[i + 3]] = FUNCTIONS.get(current).apply(firstParam, secondParam);
                i += 4;
            } else if (current == 3) {
                System.out.println("input:");
                oc[oc[i + 1]] = parseInt(reader.readLine());
                i += 2;
            } else if (current == 4) {
                System.out.println(param1Type == 0 ? oc[oc[i + 1]] : oc[i + 1]);
                i += 2;
            } else if (current == 5 || current == 6) {
                i = PREDICATES.get(current).test(firstParam, -1) ? secondParam : i + 3;
            } else if (current == 7 || current == 8) {
                oc[oc[i + 3]] = PREDICATES.get(current).test(firstParam, secondParam) ? 1 : 0;
                i += 4;
            } else {
                throw new IllegalStateException();
            }
            current = oc[i];
        }
    }
}
