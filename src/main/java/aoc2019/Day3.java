package aoc2019;

import java.awt.Point;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

import static java.nio.charset.StandardCharsets.UTF_8;

public class Day3 {

    private static final Map<Character, BiFunction<Integer, Integer, Point>> OPERATORS = Map.of(
            'R', (x, y) -> new Point(x + 1, y),
            'L', (x, y) -> new Point(x - 1, y),
            'U', (x, y) -> new Point(x, y + 1),
            'D', (x, y) -> new Point(x, y - 1)
    );

    public static void main(String[] args) throws URISyntaxException, IOException {
        String input = Files.readString(Path.of(Day3.class.getResource("/2019/advent3input.txt").toURI()), UTF_8).trim();
        String[] lines = input.split("\n[\r]?");
        Map<Point, Integer> points = getPoints(lines[0]);

        TriFunction<Point, Integer> func1 = (p, v, st) -> {
            int manDist = Math.abs(p.x) + Math.abs(p.y);
            return manDist < v ? manDist : v;
        };

        TriFunction<Point, Integer> func2 = (p, v, st) -> {
            int totalStep = st + points.get(p);
            return totalStep < v ? totalStep : v;
        };

        System.out.println(getClosestIntersection(lines[1], points, func1));
        System.out.println(getClosestIntersection(lines[1], points, func2));
    }

    private static int getClosestIntersection(String line, Map<Point, Integer> points, TriFunction<Point, Integer> func) {
        int minVal = Integer.MAX_VALUE;
        int x = 0;
        int y = 0;
        int step = 0;
        for (String s : line.split(",")) {
            char dir = s.charAt(0);
            int dist = Integer.parseInt(s.substring(1));
            for (int i = 0; i < dist; i++) {
                Point point = OPERATORS.get(dir).apply(x, y);
                x = point.x;
                y = point.y;
                step++;
                if (points.containsKey(point)) {
                    minVal = func.apply(point, minVal, step);
                }
            }
        }
        return minVal;
    }

    private static Map<Point, Integer> getPoints(String line) {
        Map<Point, Integer> points = new HashMap<>();
        int step = 0;
        int x = 0;
        int y = 0;
        for (String s : line.split(",")) {
            char dir = s.charAt(0);
            int dist = Integer.parseInt(s.substring(1));
            for (int i = 0; i < dist; i++) {
                Point point = OPERATORS.get(dir).apply(x, y);
                x = point.x;
                y = point.y;
                points.putIfAbsent(point, ++step);
            }
        }
        return points;
    }

    @FunctionalInterface
    private interface TriFunction<T, U> {
        U apply(T t, U u, U s);
    }
}
