package aoc2019;

import java.awt.*;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

public class Day10 {

    public static void main(String[] args) throws URISyntaxException, IOException {
        //        String input = Files.readString(Path.of(Day10.class.getResource("/2019/advent10input.txt").toURI()), UTF_8).trim();
        String input = ".#..#\n"
                + ".....\n"
                + "#####\n"
                + "....#\n"
                + "...##";
        String[] rows = input.split("(\\r)?\\n");
        boolean[][] matrix = new boolean[rows.length][rows[0].length()];

        List<Point> asteroids = new ArrayList<>();
        for (int i = 0; i < rows.length; i++) {
            for (int j = 0; j < rows[i].length(); j++) {
                if (rows[i].charAt(j) == '#') {
                    matrix[i][j] = true;
                    asteroids.add(new Point(j, i));
                }
            }
        }

        int[][] v = new int[matrix.length][matrix[0].length];
        int i = asteroids.stream().mapToInt(a -> {
                    int foo = foo(matrix, a);
                    v[a.y][a.x] = foo;
                    return foo;
                }
        ).max().orElseThrow(IllegalStateException::new);

        for (int[] ints : v) {
            System.out.println(Arrays.toString(ints));
        }

        System.out.println(i);
    }

    private static int foo(boolean[][] origMatrix, Point p) {
        boolean[][] matrix = new boolean[origMatrix.length][];
        for (int i = 0; i < origMatrix.length; i++) {
            matrix[i] = Arrays.copyOf(origMatrix[i], origMatrix[i].length);
        }
        int count = 0;
        for (int i = 1; i < Math.max(matrix.length, matrix[0].length); i++) {
            Set<Point> bar = bar(p, matrix.length, matrix[0].length, i);
            count += bar.stream()
                    .filter(a -> matrix[a.y][a.x])
                    .peek(a -> {
                        int verDiff = p.y - a.y;
                        int horDiff = p.x - a.x;
                        int verStep;
                        int horStep;
                        //if zero then step by one
                        //else step by diff from a to length-((length-this)%diff)
                        if (verDiff == 0) {
                            verStep = 0;
                            horStep = 1;
                        } else if (horDiff == 0) {
                            verStep = 1;
                            horStep = 0;
                        } else {
                            verStep = verDiff;
                            horStep = horDiff;
                        }
                        int x = a.x;
                        int y = a.y;
                        while (x >= 0 && x < matrix[0].length && y >= 0 && y < matrix.length) {
                            matrix[y][x] = false;
                            x += verStep;
                            y += horStep;
                        }
                    }).count();
        }
        return count;
    }

    private static Set<Point> bar(Point p, int maxX, int maxY, int range) {
        Set<Point> result = new HashSet<>();
        int minHor = p.x - range;
        int maxHor = p.x + range;
        int minVer = p.y - range;
        int maxVer = p.y + range;
        IntStream.rangeClosed(Math.max(minHor, 0), Math.min(maxHor, maxX - 1)).forEach(x -> {
            if (minVer >= 0) {
                result.add(new Point(x, minVer));
            }
            if (maxVer < maxY) {
                result.add(new Point(x, maxVer));
            }
        });
        IntStream.rangeClosed(Math.max(minVer, 0), Math.min(maxVer, maxY - 1)).forEach(y -> {
            if (minHor >= 0) {
                result.add(new Point(minHor, y));
            }
            if (maxHor < maxX) {
                result.add(new Point(maxHor, y));
            }
        });
        return result;
    }
}
