package aoc2019;

import lombok.RequiredArgsConstructor;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.nio.charset.StandardCharsets.UTF_8;

public class Day6 {

    public static void main(String[] args) throws URISyntaxException, IOException {
        String input = Files.readString(Path.of(Day6.class.getResource("/2019/advent6input.txt").toURI()), UTF_8).trim();

        Map<String, Node> nodes = new HashMap<>();
        Node root = buildTree(input, nodes);
        countDescendants(root);
        System.out.println(nodes.keySet().stream().mapToInt(k -> nodes.get(k).noOfDescendants).sum());

        Deque<Node> pathToYou = new LinkedList<>();
        Deque<Node> pathToSan = new LinkedList<>();
        getPath(pathToYou, root, nodes.get("YOU"));
        getPath(pathToSan, root, nodes.get("SAN"));

        System.out.println(countDiff(pathToYou, pathToSan));
    }

    private static int countDiff(Deque<Node> path1, Deque<Node> path2) {
        while (path1.peekFirst() != null && path1.peekFirst().equals(path2.peekFirst())) {
            path1.pollFirst();
            path2.pollFirst();
        }
        return path1.size() + path2.size();
    }

    private static boolean getPath(Deque<Node> path, Node from, Node to) {
        if (from.equals(to)) {
            return true;
        }
        for (Node child : from.children) {
            if (getPath(path, child, to)) {
                path.addFirst(from);
                return true;
            }
        }
        return false;
    }

    private static int countDescendants(Node node) {
        if (node.children.isEmpty()) {
            node.noOfDescendants = 0;
            return 1;
        }
        node.noOfDescendants = node.children.stream().mapToInt(Day6::countDescendants).sum();
        return node.noOfDescendants + 1;
    }

    private static Node buildTree(String input, Map<String, Node> nodes) {
        List<Node> list = input.lines()
                .filter(s -> {
                    String[] ids = s.split("\\)");
                    Node parent = nodes.getOrDefault(ids[0], new Node(ids[0]));
                    Node child = nodes.getOrDefault(ids[1], new Node(ids[1]));
                    parent.children.add(child);
                    nodes.put(parent.id, parent);
                    nodes.put(child.id, child);
                    return parent.id.equals("COM");
                }).map(s -> nodes.get(s.split("\\)")[0]))
                .collect(Collectors.toList());
        if (list.size() != 1) {
            throw new IllegalStateException();
        }
        return list.get(0);
    }

    @RequiredArgsConstructor
    private static class Node {
        final String id;
        Set<Node> children = new HashSet<>();
        int noOfDescendants;
    }
}
