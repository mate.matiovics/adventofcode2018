package aoc2019;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;

import static java.nio.charset.StandardCharsets.UTF_8;

public class Day1 {

    public static void main(String[] args) throws URISyntaxException, IOException {
        String input = Files.readString(Path.of(Day1.class.getResource("/2019/advent1input.txt").toURI()), UTF_8);
        System.out.println(part1(input));
        System.out.println(part2(input));
    }

    private static int part1(String input) {
        return input.lines()
                .mapToInt(s -> Integer.parseInt(s) / 3 - 2)
                .sum();
    }

    private static int part2(String input) {
        return input.lines()
                .mapToInt(Integer::parseInt)
                .map(Day1::calculateFuel)
                .sum();
    }

    private static int calculateFuel(int input) {
        int result = input / 3 - 2;
        if (result <= 0) {
            return 0;
        }
        return result + calculateFuel(result);
    }
}
