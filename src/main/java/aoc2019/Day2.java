package aoc2019;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

import static java.nio.charset.StandardCharsets.UTF_8;

public class Day2 {

    public static void main(String[] args) throws IOException, URISyntaxException {
        String input = Files.readString(Path.of(Day2.class.getResource("/2019/advent2input.txt").toURI()), UTF_8).trim();
        int[] opcodes = Arrays.stream(input.split(","))
                .mapToInt(Integer::parseInt)
                .toArray();
        System.out.println(part1(opcodes, 12, 2));
        System.out.println(part2(opcodes));
    }

    private static int part1(int[] originalOc, int noun, int verb) {
        int[] oc = Arrays.copyOf(originalOc, originalOc.length);
        oc[1] = noun;
        oc[2] = verb;
        int currentI = 0;
        int current = oc[currentI];
        while (current != 99) {
            int value;
            if (current == 1) {
                value = oc[oc[currentI + 1]] + oc[oc[currentI + 2]];
            } else if (current == 2) {
                value = oc[oc[currentI + 1]] * oc[oc[currentI + 2]];
            } else {
                throw new IllegalStateException();
            }
            oc[oc[currentI + 3]] = value;
            current = oc[currentI += 4];
        }
        return oc[0];
    }

    private static int part2(int[] input) {
        for (int i = 0; i < 100; i++) {
            for (int j = 0; j < 100; j++) {
                if (part1(input, i, j) == 19690720) {
                    return 100 * i + j;
                }
            }
        }
        return -1;
    }
}
