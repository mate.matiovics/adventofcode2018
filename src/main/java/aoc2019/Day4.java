package aoc2019;

import java.util.stream.IntStream;

public class Day4 {
    public static void main(String[] args) {
        System.out.println(part1());
        System.out.println(part2());
    }

    private static long part1() {
        return IntStream.range(248345, 746316)
                .filter(Day4::isValidPwP1)
                .count();
    }

    private static long part2() {
        return IntStream.range(248345, 746316)
                .filter(Day4::isValidPwP2)
                .count();
    }

    private static boolean isValidPwP1(int number) {
        int prev = number / 100000;
        number = number % 100000;
        boolean hasDubs = false;
        for (int i = 4; i >= 0; i--) {
            int curr = number / (int) Math.pow(10, i);
            if (prev > curr) {
                return false;
            }
            if (curr == prev) {
                hasDubs = true;
            }
            prev = curr;
            number = number % (int) Math.pow(10, i);
        }
        return hasDubs;
    }

    private static boolean isValidPwP2(int number) {
        int prev = number / 100000;
        int count = 1;
        number = number % 100000;
        boolean[] dubs = new boolean[10];
        for (int i = 4; i >= 0; i--) {
            int curr = number / (int) Math.pow(10, i);
            if (prev > curr) {
                return false;
            }
            if (curr == prev) {
                dubs[curr] = ++count <= 2;
            } else {
                count = 1;
            }
            prev = curr;
            number = number % (int) Math.pow(10, i);
        }
        for (boolean dub : dubs) {
            if(dub) {
                return true;
            }
        }
        return false;
    }
}
