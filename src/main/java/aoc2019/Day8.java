package aoc2019;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

import static java.nio.charset.StandardCharsets.UTF_8;

public class Day8 {
    public static void main(String[] args) throws URISyntaxException, IOException {
        String input = Files.readString(Path.of(Day8.class.getResource("/2019/advent8input.txt").toURI()), UTF_8).trim();
        part1(input);
        part2(input);
    }

    private static void part2(String input) {
        int width = 25;
        int height = 6;
        int[][] matrix = new int[height][width];
        for (int i = 0; i < input.length(); i += width * height) {
            for (int j = i; j < i + width * height; j++) {
                int value = Character.getNumericValue(input.charAt(j)) + 1;
                int x = (j - i) % width;
                int y = (j - i) / width;
                if (matrix[y][x] == 0 || matrix[y][x] == 3) {
                    matrix[y][x] = value;
                }
            }
        }
        for (int[] row : matrix) {
            System.out.println(Arrays.toString(row));
        }
    }

    private static void part1(String input) {
        int minZero = Integer.MAX_VALUE;
        int result = 0;
        int width = 25;
        int height = 6;
        for (int i = 0; i < input.length(); i += width * height) {
            int noOfOnes = 0;
            int noOfTwos = 0;
            int noOfZeros = 0;
            for (int j = i; j < i + width * height; j++) {
                int value = Character.getNumericValue(input.charAt(j));
                if (value == 1) noOfOnes++;
                if (value == 2) noOfTwos++;
                if (value == 0) noOfZeros++;
            }
            if (noOfZeros < minZero) {
                minZero = noOfZeros;
                result = noOfOnes * noOfTwos;
            }
        }
        System.out.println(result);
    }
}
