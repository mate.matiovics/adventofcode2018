package aoc2019;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;

import static java.nio.charset.StandardCharsets.UTF_8;

//TODO: refactor
public class Day7 {

    private static final Map<Integer, BiFunction<Integer, Integer, Integer>> FUNCTIONS = Map.of(1, Integer::sum, 2, (a, b) -> a * b);
    private static final Map<Integer, BiPredicate<Integer, Integer>> PREDICATES = Map.of(
            5, (a, b) -> a != 0, 6, (a, b) -> a == 0,
            7, (a, b) -> a < b, 8, Integer::equals
    );

    public static void main(String[] args) throws URISyntaxException, IOException {
        String input = Files.readString(Path.of(Day7.class.getResource("/2019/advent7input.txt").toURI()), UTF_8).trim();
        int[] opcodes = Arrays.stream(input.split(","))
                .mapToInt(Integer::parseInt)
                .toArray();

        solve(opcodes);
    }

    private static void solve(int[] opcodes) {
        List<String> permutation = permutation("56789");
        int maxResult = 0;
        for (String s : permutation) {
            List<Amplifier> amps = new ArrayList<>();
            for (int i = 0; i < 5; i++) {
                amps.add(new Amplifier(opcodes, Character.getNumericValue(s.charAt(i))));
            }
            for (int i = 0; i < 5; i++) {
                amps.get(i).receiver = amps.get((i + 1) % 5);
            }
            Amplifier current = amps.get(0);
            int input = current.compute(0);
            do {
                current = current.receiver;
                input = current.compute(input);
            } while (input != -1 || !current.equals(amps.get(4)));
            if (current.output > maxResult) {
                maxResult = current.output;
            }
        }

        System.out.println(maxResult);
    }

    private static List<String> permutation(String str) {
        List<String> permutations = new ArrayList<>();
        permutation(permutations, str, "");
        return permutations;
    }

    private static void permutation(List<String> list, String str, String prefix) {
        if (str.length() == 0) {
            list.add(prefix);
        } else {
            for (int i = 0; i < str.length(); i++) {
                String rem = str.substring(0, i) + str.substring(i + 1);
                permutation(list, rem, prefix + str.charAt(i));
            }
        }
    }

    private static class Amplifier {
        int[] memory;
        int phase;
        int instructionPointer = 0;
        int output = 0;
        Amplifier receiver;
        boolean phaseRead;

        Amplifier(int[] memory, int phase) {
            this.memory = Arrays.copyOf(memory, memory.length);
            this.phase = phase;
        }

        int compute(int input) {
            int current = memory[instructionPointer];
            while (current != 99) {
                int param2Type = current / 1000;
                current = current % 1000;
                int param1Type = current / 100;
                current = current % 100;
                int firstParam = 0;
                int secondParam = 0;
                if (current != 3 && current != 4) {
                    firstParam = param1Type == 0 ? memory[memory[instructionPointer + 1]] : memory[instructionPointer + 1];
                    secondParam = param2Type == 0 ? memory[memory[instructionPointer + 2]] : memory[instructionPointer + 2];
                }
                if (current == 1 || current == 2) {
                    memory[memory[instructionPointer + 3]] = FUNCTIONS.get(current).apply(firstParam, secondParam);
                    instructionPointer += 4;
                } else if (current == 3) {
                    if (!phaseRead) {
                        memory[memory[instructionPointer + 1]] = phase;
                        phaseRead = true;
                    } else {
                        memory[memory[instructionPointer + 1]] = input;
                    }
                    instructionPointer += 2;
                } else if (current == 4) {
                    output = param1Type == 0 ? memory[memory[instructionPointer + 1]] : memory[instructionPointer + 1];
                    instructionPointer += 2;
                    return output;
                } else if (current == 5 || current == 6) {
                    instructionPointer = PREDICATES.get(current).test(firstParam, -1) ? secondParam : instructionPointer + 3;
                } else if (current == 7 || current == 8) {
                    memory[memory[instructionPointer + 3]] = PREDICATES.get(current).test(firstParam, secondParam) ? 1 : 0;
                    instructionPointer += 4;
                } else {
                    throw new IllegalStateException();
                }
                current = memory[instructionPointer];
            }
            return -1;
        }
    }
}
