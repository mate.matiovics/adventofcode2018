package aoc2018;

import java.awt.*;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.function.Predicate.not;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toSet;

public class Day7 {
    public static void main(String[] args) throws URISyntaxException, IOException {
        String input = Files.readString(Paths.get(Day2.class.getResource("/2018/advent7input.txt").toURI()), StandardCharsets.UTF_8).trim();

        assemble(input, 1, 0);
        assemble(input, 5, 60);
    }

    private static void assemble(String input, int workerCount, int baseTime) {
        Map<Character, Set<Character>> prior = input.lines().collect(groupingBy(s -> s.charAt(36), mapping(s -> s.charAt(5), toSet())));
        Map<Character, Set<Character>> subsequent = input.lines().collect(groupingBy(s -> s.charAt(5), mapping(s -> s.charAt(36), toSet())));

        long taskCount = Stream.of(prior.keySet(), subsequent.keySet()).flatMap(Collection::stream).distinct().count();
        TreeSet<Character> upcoming = subsequent.keySet().stream().filter(not(prior::containsKey)).collect(Collectors.toCollection(TreeSet::new));

        Point[] workers = IntStream.range(0, workerCount).mapToObj(i -> new Point(0, 0)).toArray(Point[]::new);
        int time = 0;
        StringBuilder sb = new StringBuilder();

        while (sb.length() < taskCount) {
            for (Point worker : workers) {
                if (worker.y == 0 && !upcoming.isEmpty()) {
                    Character task = upcoming.pollFirst();
                    worker.x = task;
                    worker.y = task - 'A' + baseTime + 1;
                }
            }
            for (Point worker : workers) {
                if (worker.y > 0 && --worker.y == 0) {
                    Character task = (char) worker.x;
                    if (subsequent.containsKey(task)) {
                        for (Character dependsOn : subsequent.get(task)) {
                            Set<Character> requirements = prior.get(dependsOn);
                            requirements.remove(task);
                            if (requirements.isEmpty()) {
                                upcoming.add(dependsOn);
                            }
                        }
                    }
                    sb.append(task);
                }
            }
            time++;
        }
        System.out.println(sb);
        System.out.println(time);
    }
}
