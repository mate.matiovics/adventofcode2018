package aoc2018;

import lombok.Data;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Day10 {
    public static void main(String[] args) throws URISyntaxException, IOException {
        String input = Files.readString(Paths.get(Day2.class.getResource("/2018/advent10input.txt").toURI()), StandardCharsets.UTF_8).trim();
//                String input = "position=< 9,  1> velocity=< 0,  2>\n"
//                        + "position=< 7,  0> velocity=<-1,  0>\n"
//                        + "position=< 3, -2> velocity=<-1,  1>\n"
//                        + "position=< 6, 10> velocity=<-2, -1>\n"
//                        + "position=< 2, -4> velocity=< 2,  2>\n"
//                        + "position=<-6, 10> velocity=< 2, -2>\n"
//                        + "position=< 1,  8> velocity=< 1, -1>\n"
//                        + "position=< 1,  7> velocity=< 1,  0>\n"
//                        + "position=<-3, 11> velocity=< 1, -2>\n"
//                        + "position=< 7,  6> velocity=<-1, -1>\n"
//                        + "position=<-2,  3> velocity=< 1,  0>\n"
//                        + "position=<-4,  3> velocity=< 2,  0>\n"
//                        + "position=<10, -3> velocity=<-1,  1>\n"
//                        + "position=< 5, 11> velocity=< 1, -2>\n"
//                        + "position=< 4,  7> velocity=< 0, -1>\n"
//                        + "position=< 8, -2> velocity=< 0,  1>\n"
//                        + "position=<15,  0> velocity=<-2,  0>\n"
//                        + "position=< 1,  6> velocity=< 1,  0>\n"
//                        + "position=< 8,  9> velocity=< 0, -1>\n"
//                        + "position=< 3,  3> velocity=<-1,  1>\n"
//                        + "position=< 0,  5> velocity=< 0, -1>\n"
//                        + "position=<-2,  2> velocity=< 2,  0>\n"
//                        + "position=< 5, -2> velocity=< 1,  2>\n"
//                        + "position=< 1,  4> velocity=< 2,  1>\n"
//                        + "position=<-2,  7> velocity=< 2, -2>\n"
//                        + "position=< 3,  6> velocity=<-1, -1>\n"
//                        + "position=< 5,  0> velocity=< 1,  0>\n"
//                        + "position=<-6,  0> velocity=< 2,  0>\n"
//                        + "position=< 5,  9> velocity=< 1, -2>\n"
//                        + "position=<14,  7> velocity=<-2,  0>\n"
//                        + "position=<-3,  6> velocity=< 2, -1>";

        Pattern pattern = Pattern.compile("-?\\d+");
        List<Point> list = input.lines().map(s -> {
            int[] ints = pattern.matcher(s).results()
                    .mapToInt(r -> Integer.valueOf(r.group()))
                    .toArray();
            return new Point(ints);
        }).collect(Collectors.toList());
        int time = 0;

        while (!checkPoints(list)) {
            list.forEach(Point::move);
            time++;
        }

        print(list);
        System.out.println(time);
    }

    private static boolean checkPoints(List<Point> points) {
        //        TreeSet<Integer> vertical = points.stream().mapToInt(Point::getY).boxed().collect(Collectors.toCollection(TreeSet::new));
        //        TreeSet<Integer> horizontal = points.stream().mapToInt(Point::getX).boxed().collect(Collectors.toCollection(TreeSet::new));
        //
        //        if (!IntStream.rangeClosed(vertical.first(), vertical.last()).allMatch(vertical::contains)) {
        //            return false;
        //        }
        //        return !IntStream.rangeClosed(horizontal.first(), horizontal.last()).allMatch(horizontal::contains);
        return points.stream().mapToInt(Point::getY).distinct().count() == 10;
    }

    private static void print(List<Point> points) {
        IntSummaryStatistics vertical = points.stream().mapToInt(Point::getY).summaryStatistics();
        IntSummaryStatistics horizontal = points.stream().mapToInt(Point::getX).summaryStatistics();

        int[][] matrix = new int[vertical.getMax() - vertical.getMin() + 1][horizontal.getMax() - horizontal.getMin() + 1];
        points.forEach(p -> matrix[p.y - vertical.getMin()][p.x - horizontal.getMin()] = 1);

        for (int[] ints : matrix) {
            System.out.println(Arrays.toString(ints));
        }

    }

    @Data
    private static class Point {
        int x;
        int y;
        int velX;
        int velY;

        public Point(int... coor) {
            x = coor[0];
            y = coor[1];
            velX = coor[2];
            velY = coor[3];
        }

        public void move() {
            x += velX;
            y += velY;
        }
    }
}
