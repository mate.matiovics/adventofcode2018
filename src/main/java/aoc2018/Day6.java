package aoc2018;

import java.awt.*;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.lang.Math.abs;

public class Day6 {

    public static void main(String[] args) throws URISyntaxException, IOException {
        String input = Files.readString(Paths.get(Day2.class.getResource("/2018/advent6input.txt").toURI()), StandardCharsets.UTF_8).trim();
        findArea(input);
    }

    private static void findArea(String input) {
        List<Point> points = input.lines().map(Day6::getPoint).collect(Collectors.toList());
        int maxX = points.stream().mapToInt(p -> p.x).max().orElse(0);
        int maxY = points.stream().mapToInt(p -> p.y).max().orElse(0);
        Map<Point, Integer> counts = new HashMap<>();

        int count = 0;
        for (int i = 0; i < maxY; i++) {
            for (int j = 0; j < maxX; j++) {
                int minDist = maxX + maxY;
                Point p = null;
                int totalDist = 0;
                for (Point point : points) {
                    int dist = abs(point.x - j) + abs(point.y - i);
                    totalDist += dist;
                    if (dist < minDist) {
                        p = point;
                        minDist = dist;
                    } else if (dist == minDist) {
                        p = null;
                    }
                }
                if (totalDist < 10000) {
                    count++;
                }
                if (i == 0 || i == maxY - 1 || j == 0 || j == maxX - 1) {
                    counts.put(p, Integer.MIN_VALUE);
                }
                counts.put(p, counts.getOrDefault(p, 0) + 1);
            }
        }

        System.out.println(counts.values().stream().max(Integer::compareTo).orElse(0));
        System.out.println(count);
    }

    private static Point getPoint(String input) {
        Point point = new Point();
        point.x = Integer.valueOf(input.substring(0, input.indexOf(',')));
        point.y = Integer.valueOf(input.substring(input.indexOf(',') + 2));
        return point;
    }
}
