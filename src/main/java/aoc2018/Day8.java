package aoc2018;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Queue;
import java.util.stream.IntStream;

import static java.util.Arrays.stream;

public class Day8 {
    public static void main(String[] args) throws URISyntaxException, IOException {
        String input = Files.readString(Paths.get(Day2.class.getResource("/2018/advent8input.txt").toURI()), StandardCharsets.UTF_8).trim();

        Queue<Integer> queue = new ArrayDeque<>();
        Arrays.stream(input.split(" ")).forEach(s -> queue.add(Integer.valueOf(s)));
        System.out.println(sumMetadata(new ArrayDeque<>(queue), 0));

        ArrayList<Integer> list = new ArrayList<>();
        totalNodeCount(list, queue);
        System.out.println(list.get(0));
    }

    @SuppressWarnings("ConstantConditions")
    private static int sumMetadata(Queue<Integer> queue, int sum) {
        int nodeCount = queue.poll();
        int mdCount = queue.poll();
        for (int i = 0; i < nodeCount; i++) {
            sum = sumMetadata(queue, sum);
        }
        for (int i = 0; i < mdCount; i++) {
            sum += queue.poll();
        }
        return sum;
    }

    @SuppressWarnings("ConstantConditions")
    private static int totalNodeCount(ArrayList<Integer> list, Queue<Integer> queue) {
        int value = 0;
        int index = list.size();
        list.add(value);
        int nodeCount = queue.poll();
        int mdCount = queue.poll();
        int[] subNodes = new int[nodeCount + 1];
        for (int i = 1; i <= nodeCount; i++) {
            subNodes[i] = totalNodeCount(list, queue) + 1;
        }
        if (nodeCount == 0) {
            value += IntStream.range(0, mdCount).map(i -> queue.poll()).sum();
        } else {
            value += IntStream.range(0, mdCount).map(i -> queue.poll())
                    .filter(node -> node <= nodeCount)
                    .map(node -> list.get(index + stream(subNodes, 0, node).sum() + 1))
                    .sum();
        }
        list.set(index, value);
        return stream(subNodes).sum();
    }

    /* The above method with for loops instead of streams

    @SuppressWarnings("ConstantConditions")
    private static int totalNodeCount(ArrayList<Integer> list, Queue<Integer> queue) {
        int value = 0;
        int index = list.size();
        list.add(value);
        int nodeCount = queue.poll();
        int mdCount = queue.poll();
        int[] subNodes = new int[nodeCount + 1];
        for (int i = 1; i <= nodeCount; i++) {
            subNodes[i] = totalNodeCount(list, queue) + 1;
        }
        if (nodeCount == 0) {
            for (int i = 0; i < mdCount; i++) {
                value += queue.poll();
            }
        } else {
            for (int i = 0; i < mdCount; i++) {
                Integer node = queue.poll();
                if (node <= nodeCount) {
                    int padding = 1;
                    for (int j = 0; j < node; j++) {
                        padding += subNodes[j];
                    }
                    value += list.get(index + padding);
                }
            }
        }
        list.set(index, value);
        return Arrays.stream(subNodes).sum();
    }
    */
}
