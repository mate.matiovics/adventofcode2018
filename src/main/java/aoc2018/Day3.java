package aoc2018;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

public class Day3 {
    private static final int COLUMN = 1;
    private static final int ROW = 2;
    private static final int WIDTH = 3;
    private static final int HEIGHT = 4;

    private static Map<String, Integer> areaOccurrence = new HashMap<>();

    public static void main(String[] args) throws URISyntaxException, IOException {
        String input = Files.readString(Paths.get(Day2.class.getResource("/2018/advent3input.txt").toURI()), StandardCharsets.UTF_8);
        input.lines().forEach(Day3::processClaim);

        System.out.println(part1());
        part2(input).ifPresent(System.out::println);
    }

    private static long part1() {
        return areaOccurrence.values().stream()
                .filter(i -> i > 1)
                .count();
    }

    private static Optional<String> part2(String input) {
        return input.lines()
                .filter(Predicate.not(Day3::processClaim))
                .findFirst();
    }

    private static boolean processClaim(String claim) {
        int[] data = Arrays.stream(claim.replaceFirst("\\D", "").split("\\D+"))
                .mapToInt(Integer::valueOf)
                .toArray();

        boolean covered = false;

        for (int i = data[COLUMN]; i < data[COLUMN] + data[WIDTH] ; i++) {
            for (int j = data[ROW]; j < data[ROW] + data[HEIGHT]; j++) {
                String key = String.valueOf(i) + "," + j;
                Integer occurrence = areaOccurrence.getOrDefault(key, 0);
                if (occurrence > 1) {
                    covered = true;
                }
                areaOccurrence.put(key, occurrence + 1);
            }
        }
        return covered;
    }
}
