package aoc2018;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day2 {

    public static void main(String[] args) throws URISyntaxException, IOException {
        String input = Files.readString(Paths.get(Day2.class.getResource("/2018/advent2input.txt").toURI()), StandardCharsets.UTF_8);
        part1(input);
        part2(input);
    }

    private static void part1(String input) {
        long count = input.lines().filter(s -> hasNoccurence(s, 2)).count();
        long count2 = input.lines().filter(s -> hasNoccurence(s, 3)).count();

        System.out.println(count * count2);
    }

    private static void part2(String input) {
        List<String> list = input.lines().filter(s ->
                input.lines().anyMatch(s1 -> getCommon(s, s1).length() == s1.length() - 1))
                .collect(Collectors.toList());

        System.out.println(getCommon(list.get(0), list.get(1)));
    }

    private static boolean hasNoccurence(String s, int n) {
        return s.chars()
                .boxed()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .values().stream()
                .anyMatch(i -> i == n);
    }

    private static String getCommon(String s1, String s2) {
        StringBuilder sb = new StringBuilder();
        IntStream.range(0, s1.length())
                .filter(i -> s1.charAt(i) == s2.charAt(i))
                .forEach(i -> sb.append(s1.charAt(i)));

        return sb.toString();
    }
}
