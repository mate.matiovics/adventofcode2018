package aoc2018;

import lombok.RequiredArgsConstructor;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.AbstractMap;
import java.util.ArrayDeque;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.lang.Integer.valueOf;

public class Day4 {

    @SuppressWarnings("ConstantConditions")
    public static void main(String[] args) throws URISyntaxException, IOException {
        String input = Files.readString(Paths.get(Day2.class.getResource("/2018/advent4input.txt").toURI()), StandardCharsets.UTF_8);
        Queue<String> orderedInput = input.lines().sorted().collect(Collectors.toCollection(ArrayDeque::new));

        Pattern pattern = Pattern.compile("#\\d+");
        Map<String, Guard> guards = new HashMap<>();

        while (!orderedInput.isEmpty()) {
            String s = orderedInput.poll();
            Matcher matcher = pattern.matcher(s);
            matcher.find();
            String group = matcher.group();
            Guard guard = guards.getOrDefault(group, new Guard(group));

            while (!orderedInput.isEmpty() && !orderedInput.peek().contains("#")) {
                int fallsAsleepAt = valueOf(orderedInput.poll().substring(15, 17));
                int wakesAt = valueOf(orderedInput.poll().substring(15, 17));
                guard.totalMinutesSlept += wakesAt - fallsAsleepAt;
                for (int i = fallsAsleepAt; i < wakesAt; i++) {
                    guard.minuteFrequency.put(i, guard.minuteFrequency.getOrDefault(i, 0) + 1);
                }
            }
            guards.put(group, guard);
        }

        Guard sleepy = guards.values().stream()
                .max(Comparator.comparingInt(g -> g.totalMinutesSlept))
                .orElse(null);
        System.out.println(valueOf(sleepy.id.substring(1)) * sleepy.getTopMinute().getKey());

        Guard narcoleptic = guards.values().stream()
                .max(Comparator.comparingInt(g -> g.getTopMinute().getValue()))
                .orElse(null);
        System.out.println(valueOf(narcoleptic.id.substring(1)) * narcoleptic.getTopMinute().getKey());
    }

    @RequiredArgsConstructor
    private static class Guard {
        private final String id;
        private int totalMinutesSlept;
        private Map<Integer, Integer> minuteFrequency = new HashMap<>();
        private Map.Entry<Integer, Integer> topMinute = null;

        Map.Entry<Integer, Integer> getTopMinute() {
            if (topMinute == null) {
                topMinute = minuteFrequency.entrySet().stream().max(Comparator.comparingInt(Map.Entry::getValue))
                        .orElseGet(() -> new AbstractMap.SimpleEntry<>(0, 0));
            }
            return topMinute;
        }
    }
}
