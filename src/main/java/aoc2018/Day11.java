package aoc2018;

import java.util.Arrays;

public class Day11 {
    private static final int SERIAL = 5153;
    private static final int LENGTH = 300;

    public static void main(String[] args) {
        System.out.println(Arrays.toString(part1()));
        System.out.println(Arrays.toString(part2()));
    }

    private static int[] part1() {
        int[] cells = new int[LENGTH * LENGTH - 1];

        int[] result = new int[2];
        int maxTotal = 0;
        for (int y = 1; y <= LENGTH - 3; y++) {
            for (int x = 1; x <= LENGTH - 3; x++) {
                int sum = 0;
                for (int i = y; i < y + 3; i++) {
                    for (int j = x; j < x + 3; j++) {
                        int index = index(j, i);
                        if (cells[index] == 0) {
                            cells[index] = ((j + 10) * i + SERIAL) * (j + 10) / 100 % 10 - 5;
                        }
                        sum += cells[index];
                    }
                }
                if (sum > maxTotal) {
                    maxTotal = sum;
                    result[0] = x;
                    result[1] = y;
                }
            }
        }
        return result;
    }

    private static int[] part2() {
        int[] cells = new int[LENGTH * LENGTH - 1];

        int[] result = new int[3];
        int maxTotal = 0;
        for (int y = 1; y <= LENGTH; y++) {
            for (int x = 1; x <= LENGTH; x++) {
                int limit = Math.max(y, x);
                int sum = calculate(x, y);
                for (int k = 0; k < LENGTH - limit; k++) {
                    for (int i = 0; i < k; i++) {
                        int i1 = index(x + k, y + i);
                        if (cells[i1] == 0) {
                            cells[i1] = calculate(x + k, y + i);
                        }
                        int i2 = index(x + i, y + k);
                        if (cells[i2] == 0) {
                            cells[i2] = calculate(x + i, y + k);
                        }
                        sum += cells[i1];
                        sum += cells[i2];
                    }
                    int i3 = index(x + k, y + k);
                    if (cells[i3] == 0) {
                        cells[i3] = calculate(x + k, y + k);
                    }
                    sum += cells[i3];
                    if (sum > maxTotal) {
                        maxTotal = sum;
                        result[0] = x;
                        result[1] = y;
                        result[2] = k + 1;
                    }
                }
            }
        }
        return result;
    }

    private static int index(int x, int y) {
        return (y - 1) * LENGTH + (x - 1);
    }

    private static int calculate(int x, int y) {
        return ((x + 10) * y + SERIAL) * (x + 10) / 100 % 10 - 5;
    }
}
