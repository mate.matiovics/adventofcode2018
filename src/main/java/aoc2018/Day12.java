package aoc2018;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Day12 {
    public static void main(String[] args) {
        String initial = "....##.#....#..#......#..######..#.####.....#......##.##.##...#..#....#.#.##..##.##.#.#..#.#....#.#..#.#....";
        String input = "#.#.. => .\n"
                + "..##. => .\n"
                + "...#. => .\n"
                + "..#.. => .\n"
                + "##### => #\n"
                + ".#.#. => .\n"
                + "####. => .\n"
                + "###.. => .\n"
                + ".#..# => #\n"
                + "#..#. => #\n"
                + "#.#.# => .\n"
                + "#...# => #\n"
                + "..### => .\n"
                + "...## => #\n"
                + "##..# => #\n"
                + "#.... => .\n"
                + ".#.## => #\n"
                + "#.### => #\n"
                + ".##.# => #\n"
                + "#..## => .\n"
                + ".#... => #\n"
                + ".###. => .\n"
                + "##... => #\n"
                + "##.## => #\n"
                + "##.#. => #\n"
                + "#.##. => #\n"
                + ".##.. => .\n"
                + "..#.# => .\n"
                + "....# => .\n"
                + "###.# => .\n"
                + "..... => .\n"
                + ".#### => .";

        String regex = input.lines().filter(s -> s.charAt(9) == '#').map(s -> s.substring(0, 5).replaceAll("\\.", "\\\\.")).collect(Collectors.joining("|"));
        //        Map<Boolean, Set<String>> map = input.lines().collect(Collectors.partitioningBy(s -> s.charAt(9) == '#', Collectors.mapping(s -> s.substring(0, 5), Collectors.toSet())));
        Pattern pattern = Pattern.compile(regex);
        StringBuilder sb;
        int offset = -4;
        int sum = 0;
        for (long i = 0; i < 20L; i++) {
            int[] chars = new int[initial.length()];
            int index = 0;
            Matcher matcher = pattern.matcher(initial);
            while (matcher.find(index)) {
                int start = matcher.start();
                chars[start + 2] = 1;
                index = start + 1;
            }
            sb = new StringBuilder();
            for (int aChar : chars) {
                if (aChar == 1) {
                    sb.append('#');
                } else {
                    sb.append(".");
                }
            }
            while (sb.indexOf("#") < 4) {
                    sb.insert(0, '.');
                    offset--;
            }
            while (sb.indexOf("#") > 4 && offset < -4) {
                sb.deleteCharAt(0);
                offset++;
            }
            while (sb.lastIndexOf("#") != sb.length() - 5) {
                if (sb.lastIndexOf("#") > sb.length() - 5) {
                    sb.append(".");
                } else {
                    sb.deleteCharAt(sb.length() - 1);
                }
            }
            if (sb.toString().equals(initial)) {
                System.out.println("duplicate");
//                System.out.println(sb);
                break;
            }
            initial = sb.toString();
//            System.out.println(sb);
        }
        for (int i = 0; i < initial.length(); i++) {
            if (initial.charAt(i) == '#') {
                sum += offset + i;
            }
        }
        System.out.println(sum);
    }
}
