package aoc2018;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Stack;

import static java.lang.Character.toLowerCase;
import static java.lang.Character.toUpperCase;

public class Day5 {

    public static void main(String[] args) throws URISyntaxException, IOException {
        String input = Files.readString(Paths.get(Day2.class.getResource("/2018/advent5input.txt").toURI()), StandardCharsets.UTF_8).trim();
        System.out.println(part1(input));
        System.out.println(part2(input));
    }

    private static int part2(String input) {
        int min = input.length();
        for (char i = 'a'; i < 'z'; i++) {
            String regex = String.format("%s|%s", i, toUpperCase(i));
            int i1 = part1(input.replaceAll(regex, ""));
            if (i1 < min) {
                min  = i1;
            }
        }
        return min;
    }

    private static int part1(String input) {
        Stack<Character> stack = new Stack<>();
        for (char c : input.toCharArray()) {
            if (stack.empty())
                stack.push(c);
            else {
                Character current = c;
                Character previous = stack.peek();
                if (current != previous && toUpperCase(current) == toUpperCase(previous)) {
                    stack.pop();
                } else {
                    stack.push(current);
                }
            }
        }
        return stack.size();
    }
}
