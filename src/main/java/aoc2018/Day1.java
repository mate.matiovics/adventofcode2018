package aoc2018;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Set;

import static java.nio.charset.StandardCharsets.UTF_8;

public class Day1 {

    public static void main(String[] args) throws IOException, URISyntaxException {
        String input = Files.readString(Path.of(Day1.class.getResource("/2018/advent1input.txt").toURI()), UTF_8);
        System.out.println(part1(input));
        System.out.println(part2(input));
    }

    private static int part2(String input) {
        String[] split = input.lines().toArray(String[]::new);

        int i = 0;
        Set<Integer> set = new HashSet<>();
        int sum = 0;
        while (!set.contains(sum)) {
            set.add(sum);
            i = i > split.length - 1 ? i % split.length : i;
            sum += Integer.parseInt(split[i++]);
        }
        return sum;
    }

    private static int part1(String input) {
        return input.lines()
                .mapToInt(Integer::valueOf)
                .sum();
    }
}
