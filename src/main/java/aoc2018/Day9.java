package aoc2018;

public class Day9 {

    public static void main(String[] args) {
        System.out.println(playWithMarbles(459, 71320));
        System.out.println(playWithMarbles(459, 7132000));
    }

    private static long playWithMarbles(int playerCount, int marbleCount) {
        long[] players = new long[playerCount];
        long highScore = 0;
        int value = 0;
        int player = 0;

        Marble current = new Marble(null, null, value);
        while (value <= marbleCount) {
            if (value % 23 == 0) {
                Marble toRemove = current.prev.prev.prev.prev.prev.prev.prev;
                toRemove.next.prev = toRemove.prev;
                toRemove.prev.next = toRemove.next;
                current = toRemove.next;

                players[player] += toRemove.value;
                players[player] += value++;
                if (players[player] > highScore) {
                    highScore = players[player];
                }
            } else {
                Marble newMarble = new Marble(current.next.next, current.next, value++);
                current.next.next.prev = newMarble;
                current.next.next = newMarble;
                current = newMarble;
            }
            player = ++player > players.length - 1 ? player % players.length : player;
        }
        return highScore;
    }

    private static class Marble {
        Marble next;
        Marble prev;
        int value;

        Marble(Marble next, Marble prev, int value) {
            this.next = next == null ? this : next;
            this.prev = prev == null ? this : prev;
            this.value = value;
        }
    }
}
